import os
from pathlib import Path
import glob
import concurrent.futures

import matplotlib.pyplot as plt
import numpy as np

import helpers

# Static definitions
DATA_PATH = "data/"
# OPTIONS:
# color = https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmatplotlib.org%2F3.1.0%2F_images%2Fsphx_glr_named_colors_003.png&f=1&nofb=1&ipt=c09576603c011e4da5c9a465cdbb9511e7f649e82443b21ed17676113a1762bf&ipo=images
# linesyle = Meh to ve seen
LINESTYLE = [
    {'linestyle': 'dotted', 'linewidth': 1.5, 'color': 'darkmagenta'},
    {'linestyle': 'dashed', 'linewidth': 1.5, 'color': 'chartreuse'},
    {'linestyle': 'dashdot', 'linewidth': 1.5, 'color': 'orangered'},
    {'linestyle': 'solid', 'linewidth': 1.5, 'color': 'darkturquoise'},
    {'linestyle': 'dotted', 'linewidth': 1.5, 'color': 'darkmagenta'},
    {'linestyle': 'dashed', 'linewidth': 1.5, 'color': 'chartreuse'},
    {'linestyle': 'dashdot', 'linewidth': 1.5, 'color': 'orangered'},
    {'linestyle': 'solid', 'linewidth': 1.5, 'color': 'darkturquoise'},
]


# Create the plots dir structure
Path("plots/thesis").mkdir(parents=True, exist_ok=True)
Path("plots/paper").mkdir(parents=True, exist_ok=True)

def process_csv(path):
    config = helpers.read_config_file(path.replace('.csv', '.json'))

    # Load the CSV file, skipping the first row and using the second row for units
    data = np.genfromtxt(path, delimiter=',', skip_header=1, dtype=None, encoding='utf-8')
    units = data[0]  # Second row for units
    data = data[1:]  # Data starts from the third row

    # Convert the first column (time) from seconds to nanoseconds
    time_ns = data[:, 0].astype(float) * 1e9

    import matplotlib.pyplot as plt
    # Configure the graph
    fig, ax1 = plt.subplots()
    ax1.minorticks_on() 
    ax1.grid(True)
    ax1.set_xlabel(f't [ns]')
    ax1.set_ylabel(f'U [V]')
    # print trigger line
    x_index = helpers.get_index_of_closest_value(data[:, 0], 0)
    print(os.path.basename(path) + " >>> Trigger Value => " + str(data[x_index, 1]))
    ax1.axhline(y=float(data[x_index,1]), color='red', linestyle='solid',
                linewidth=0.3) #, label=f'trigger')
    # print trigger signal
    x_text = config.get('window')[0]
    ax1.text(x_text, float(data[x_index,1]), 'T ►', verticalalignment='center',
             horizontalalignment='right', fontsize=10, color='red')

    # Do we specify second axis values?
    ax2 = None
    if len(config.get('second_axis')) > 0:
        ax2 = ax1.twinx()
        ax2.set_ylabel(f'U [V]')

    # Extract and plot the data lines
    plot = None
    sa = config.get('second_axis')
    for c in range(data.shape[1]-1):
        d = data[:, c+1].astype(float)
        if sa and c + 1 in sa:
            plt = ax2
        else:
            plt = ax1
        plt.plot(time_ns, d, label=f'{units[c+1]}', **LINESTYLE[c])
        # get time for U=?
        if config.get('measure') and len(config.get('search')) >= c:
            # plt measure line low
            y_index = helpers.get_index_of_closest_value(data[:, c+1], config.get('search')[c][0])
            print(os.path.basename(path) + " >>> Values for U" + str(c) + " = " + str(data[y_index,0]) + ": " + str(data[y_index, c+1]))
            plt.axvline(x=time_ns[y_index], color='blue', linestyle='solid', linewidth=0.3)
            # plt measure line high
            y_index = helpers.get_index_of_closest_value(data[:, c+1], config.get('search')[c][1])
            print(os.path.basename(path) + " >>> Values for U" + str(c) + " = " + str(data[y_index,0]) + ": " + str(data[y_index, c+1]))
            plt.axvline(x=time_ns[y_index], color='blue', linestyle='solid', linewidth=0.3)

    window = config.get('window')
    ax1.set_xlim(left=window[0], right=window[1])
    if len(config.get('second_axis')) > 0:
        ax2.set_xlim(left=window[0], right=window[1])
    fig.legend()

    # Save pictures
    picture_name = os.path.basename(path).replace('.csv', '.png')
    #plt.style.use('_mpl-gallery')
    fig.savefig('plots/paper/'+ picture_name, format='png', bbox_inches='tight')
    #plt.style.use('_classic_test_patch')
    fig.savefig('plots/thesis/'+ picture_name, format='png', bbox_inches='tight')
    fig.show()


# Read csv folders and gererate plots for all charts
pattern = os.path.join(DATA_PATH, '*.csv')
# Use glob to find all files that match the pattern
csv_files = glob.glob(pattern)
for file_path in csv_files:
    process_csv(file_path)

## FOR SOME REASON CONCURRENCY BREAKS THE matplotlib.pyplot.text arrow...
## Generate the charts
#with concurrent.futures.ThreadPoolExecutor() as executor:
#    # Map the process_csv function to each file path
#    futures = [executor.submit(process_csv, file_path) for file_path in csv_files]
#
#    # Wait for all the futures to complete
#    for future in concurrent.futures.as_completed(futures):
#        try:
#            future.result()  # This will raise an exception if the function raised one
#        except Exception as e:
#            print(f"An error occurred: {e}")



print("All Done...")
